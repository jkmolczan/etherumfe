import React, { Component } from 'react';
import {connect} from 'react-redux';
import EtherumAvgLabel from './EtherumAvgLabel';
import {fetchAvgTime} from './../actions';

import './../styles/EtherumBox.css';

class EtherumBox extends Component {
  componentDidMount() {
      setInterval(() => {
        this.props.getAvgTime();
      }, 10000);

  }

  render() {
    return (
      <div className="etherum-box">
          <header>
              <img src="https://etoro-cdn.etorostatic.com/market-avatars/eth-usd/150x150.png" />
              <div>ETHERUM</div>
          </header>
          <main>
              <div>Avg block time:</div>
              <EtherumAvgLabel/>
          </main>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAvgTime: () => dispatch(fetchAvgTime())
    };
};

export default connect(null, mapDispatchToProps)(EtherumBox);
