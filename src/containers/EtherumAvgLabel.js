import {connect} from 'react-redux';
import Label from '../components/Label';

const mapStateToProps = (state) => {
    return {
        value: state.avgTime
    };
};

export default connect(mapStateToProps)(Label);
