import {UPDATE_AVG_TIME} from './actions';

const initialState = {
  avgTime: null
};

function reducer(state=initialState, action) {
    switch (action.type) {
        case UPDATE_AVG_TIME:
            return {...state, avgTime: action.avgTime};
        default:
            return state
    }
}

export default reducer;