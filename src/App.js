import React, { Component } from 'react';
import EtherumBox from './containers/EtherumBox';

class App extends Component {
  render() {
    return (
      <div>
          <EtherumBox/>
      </div>
    );
  }
}

export default App;
