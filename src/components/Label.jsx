import React from 'react';
import './../styles/Label.css';

function Label({value}) {
    return (
        <span className="label">
            {value}
        </span>
    );
}

export default Label;