import axios from 'axios';

export const UPDATE_AVG_TIME = 'UPDATE_AVG_TIME ';
export const updateAvgTime = (avgTime) => {
    return {
        type: UPDATE_AVG_TIME,
        avgTime
    };
}

export const fetchAvgTime = () => {

    return (dispatch) => {
        axios.get('http://localhost:8080')
            .then((response) => {
                dispatch(updateAvgTime(response.data));
            });
    };
}

