# Etherum Blockchain Calculator Frontend.

This is a little demo app displaying an average time of adding a block to the ethereum blockchain.

You could run the app using machine dependnecies.

## Running using local machine

### Installing

Check out the project and run:

```sh
npm install
```

## Running

### Using the app

```sh
npm start
```

### Running using docker

```sh
docker-compose up
```

You should now have the app server at [localhost:3000](http://localhost:3000).


### TODO list:
* Tests
* Logging
